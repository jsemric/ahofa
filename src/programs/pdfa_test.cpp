/// Jakub Semric
///
/// Unless otherwise stated, all code is licensed under a
/// GNU General Public Licence v2.0

#include <vector>
#include <cctype>
#include <boost/program_options.hpp>
#include <system_error>
#include "../automata_learning/pdfa.h"
#include "../pcap_reader/pcap_reader.h"

std::string help_str =
"usage: traffic_processing --automaton FILE --pcap PCAP [OPTIONS]\n"
"Program for processing the packets through probabilistic automaton. Prints an average value "
"and a variance over the acquired probabilities. It is also possible to open several pcap files. "
"If no automaton is specified then the program prints payload in readable format.\n\n";

template<typename F>
void read_pcaps(const std::vector<std::string> &pcaps, unsigned long count, F func) {

    PcapReader pcap_reader;

    for (auto &p : pcaps) {
        try {
            pcap_reader.open(p);
            pcap_reader.process_packets(func, count);
        }
        catch (std::system_error) {
            std::cerr << "Error: cannot open pcap file " << p << "\n";
        }

    }
}

int main(int argc, char **argv) {

    namespace po = boost::program_options;

    // handle arguments
    po::options_description desc("Allowed options");
    desc.add_options()
    ("help,h", "show this help and exit")
    ("pcap,p", po::value<std::vector<std::string>>()->value_name("PCAP"), "pcap file")
    ("count,c", po::value<unsigned long>()->value_name("COUNT"), "number of packets with some data")
    ("automaton,a", po::value<std::string>()->value_name("FILE"), "file containing PDFA");

    po::variables_map varmap;
    try {
        po::store(po::parse_command_line(argc, argv, desc), varmap);
        po::notify(varmap);
    }
    catch (po::error &e) {
        std::cerr << "Error: " << e.what() << std::endl;
        return 1;
    }

    if (argc == 1 || varmap.count("help")) {
        std::cout << help_str;
        std::cout << desc << std::endl;
        return 0;
    }

    if (varmap.count("pcap") == 0) {
        std::cerr << "Error: pcap file did not specified\n";
        return 1;
    }

    // setting parameters and pcap files
    unsigned long count = varmap.count("count") ? varmap["count"].as<unsigned long>() : ULONG_MAX;
    std::vector<std::string> pcaps = varmap["pcap"].as<std::vector<std::string>>();

    PDFA pdfa{255};
    std::vector<long double> probs;

    if (varmap.count("automaton")) {
        std::ifstream input{varmap["automaton"].as<std::string>()};
        if (!input.is_open()) {
            std::cerr << "Error: cannot open " << varmap["automaton"].as<std::string>() << "\n";
        }

        try {
            pdfa.read(input);
        }
        catch (FiniteAutomatonException &e) {
            std::cerr << "Error: " << e.what() << std::endl;
            return 1;
        }

        input.close();
        // counting the probabilities
        read_pcaps(pcaps, count, [& pdfa, & probs](const unsigned char* payload, unsigned length) {
                probs.push_back(pdfa.accept(payload, length));
            });

        long double sum = 0;
        long double var = 0;
        for (auto i : probs) {
            sum += i;
            var += i*i;
        }

        long double avg = sum/probs.size();
        var -= avg*avg;

        std::cout << "Number of pcaps: " << probs.size() << "\n";
        std::cout << "Average: " << avg << "\n";
        std::cout << "Variance: " << var << "\n";
    }
    else {
        // just read and print payload in readable format
        read_pcaps(pcaps, count, [] (const unsigned char* payload, unsigned length) {
                for (unsigned i = 0; i < length; i++) {
                    printf("%c", (std::isprint(payload[i]) ? payload[i] : '.'));
                }
                printf("\n");
            });
    }

    return 0;
}
