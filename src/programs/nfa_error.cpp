#include <iostream>
#include <exception>
#include <vector>
#include <boost/program_options.hpp>
#include <system_error>
#include "../automata_learning/nfa.h"
#include "../pcap_reader/pcap_reader.h"

namespace po = boost::program_options;

const char *help_str =
"usage: nfa_error [OPTIONS]\n"
"Program for comparing reduced NFAs with target NFA. Reduced NFAs are supposed to over-approximate "
"language accepted by target NFA.\n\n";

void compute_frequencies(std::string pcap, const std::string &target_str, std::ostream &out)
{
    std::ifstream input{target_str};

    if (!input.is_open()) {
        throw 2;
    }

    NFA nfa;
    nfa.read_from_file(input);
    nfa.compute_depth();
    PcapReader pcap_reader;
    pcap_reader.open(pcap);
    pcap_reader.process_packets(
    [&nfa](const unsigned char* payload, unsigned length)
    {
        nfa.compute_frequency(payload, length);
    }, ~0LL);

    nfa.print(out);
    out << "=================\n";
    nfa.print_freq(out);
}

void compute_error(std::string pcap, const std::string &target_str,
                   const std::vector<std::string> &hyp_str, std::ostream &out)
{
    std::ifstream input{target_str};

    if (!input.is_open()) {
        throw 2;
    }

    std::string stats;
    std::vector<NFA> nfas;
    std::vector<unsigned long> positive(hyp_str.size() + 1);
    unsigned long total = 0;

    nfas.push_back(NFA{});
    nfas[0].read_from_file(input);
    input.close();
    for (size_t i = 0; i < hyp_str.size(); i++) {
        nfas.push_back(NFA{});
        std::ifstream input{hyp_str[i]};

        if (!input.is_open()) {
            throw 2;
        }
        nfas[i+1].read_from_file(input);
        input.close();
    }

    PcapReader pcap_reader;
    pcap_reader.open(pcap);
    pcap_reader.process_packets(
    [&positive, &total, &nfas](const unsigned char* payload, unsigned length)
    {
        total++;
        for (size_t i = 0; i < nfas.size(); i++) {
            positive[i] += nfas[i].accept(payload, length);
        }
    }, ~0LL);

    unsigned long target_positive = positive[0];
    out << "NFA: " <<  target_str << "\n";
    out << "Total: " << total << "\n";
    out << "Accepted: " << target_positive << "\n";
    for (size_t i = 0; i < hyp_str.size(); i++) {
        out << "===========\n";
        out << "NFA: " << hyp_str[i] << "\n";
        out << "Accepted: " << positive[i+1] << "\n";
        out << "Error: " << 1.0*(positive[i+1] - target_positive) / total << "\n";
    }
}

void conflict_options(const po::variables_map &vm, const std::string &opt1,
                      const std::string &opt2)
{
    if (vm.count(opt1) && !vm[opt1].defaulted() && vm.count(opt2) && !vm[opt2].defaulted())
    {
        throw std::logic_error(std::string("Conflicting options '") + opt1 + "' and '" + opt2
                               + "'.");
    }
}

int main(int argc, char **argv) {

    std::string target_str, out, pcap;
    std::vector<std::string> hyp_str;

    po::options_description desc("Allowed options");
    desc.add_options()
    ("help,h", "show this help and exit")
    ("pcap,p", po::value(&pcap)->value_name("PCAP")->required(), "pcap file")
    ("target,t", po::value(&target_str)->value_name("NFA1")->required(), "target nfa file")
    ("automaton,a", po::value(&hyp_str)->value_name("NFA2"), "nfa files to test error")
    ("frequencies,f", "compute frequencies of NFA")
    ("output,o", po::value(&out)->value_name("FILE"), "output file");

    po::variables_map varmap;
    try {
        po::store(po::parse_command_line(argc, argv, desc), varmap);
        if (argc == 1 || varmap.count("help")) {
            std::cout << help_str;
            std::cout << desc << std::endl;
            return 0;
        }
        po::notify(varmap);
        conflict_options(varmap, "frequencies", "automaton");
    }
    catch (po::error &e) {
        std::cerr << "Error: " << e.what() << std::endl;
        return 1;
    }

    std::string stats;

    try {
        std::ostream *output = &std::cout;
        if (varmap.count("output")) {
            output = new std::ofstream{out};
        }

        if (varmap.count("frequencies")) {
            compute_frequencies(pcap, target_str, *output);
        }
        else {
            compute_error(pcap, target_str, hyp_str, *output);
        }
        if (varmap.count("output")) {
            static_cast<std::ofstream*>(output)->close();
            delete output;
        }
    }
    catch (std::exception &e) {
        std::cerr << "Error: " << e.what() << std::endl;
        return 3;
    }

    return 0;
}
