/// Jakub Semric
///
/// Unless otherwise stated, all code is licensed under a
/// GNU General Public Licence v2.0

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <cctype>
#include <boost/program_options.hpp>
#include "../pcap_reader/pcap_reader.h"
#include "../automata_learning/ffa.h"
#include "../automata_learning/pdfa.h"
#include "../automata_learning/alergia.h"
#include "../automata_learning/aux.h"

std::string help_str =
"usage: traffic_learning --pcap PCAP [OPTIONS]\n"
"Program for constructing probabilistic automaton (PA) from network traffic.\n\n";

namespace po = boost::program_options;

void make_chain(PcapReader &pcap_reader,FDFA &ffa, unsigned long count,
                unsigned state_count)
{
    unsigned long s1 = ffa.get_initial_state();
    unsigned long s2 = s1;
    for (unsigned i = 1; i < state_count; i++, s1 = s2) {
        s2 = ffa.add_state();
        ffa.set_final_frequency(s1, 1);
        for (unsigned j = 0; j < ffa.get_alphabet_size(); j++) {
            ffa.add_transition(s1, j, s2, 1);
        }
    }

    for (unsigned j = 0; j < ffa.get_alphabet_size(); j++) {
        ffa.set_final_frequency(s2, 1);
        ffa.add_transition(s2, j, s2, 1);
    }

    pcap_reader.process_packets(
        [&ffa](const unsigned char* payload, unsigned length) {
            ffa.augment(payload, length);
        }, count);
}

void make_prefix_tree(PcapReader &pcap_reader,FDFA &ffa, unsigned long count,
                      unsigned long max)
{
    if (max == 0) {
        // first parameter is lambda, in this case it builds a prefix tree acceptor from samples
        // second is total count of packets
        pcap_reader.process_packets(
            [&ffa](const unsigned char* payload, unsigned length) {
                ffa.augment(payload, length);
            }, count);
    }
    else {
        // learning on samples of specific maximal length
        pcap_reader.process_packets(
            [&ffa, max](const unsigned char* payload, unsigned length) {
                ffa.augment(payload, length > max ? max : length);
            }, count);
    }
}

void conflict_options(const po::variables_map &vm, const std::string &opt1,
                      const std::string &opt2)
{
    if (vm.count(opt1) && !vm[opt1].defaulted() && vm.count(opt2) && !vm[opt2].defaulted())
    {
        throw std::logic_error(std::string("Conflicting options '") + opt1 + "' and '" + opt2
                               + "'.");
    }
}

int main(int argc, char **argv) {

    double alpha;
    unsigned threshold, state_count;
    unsigned long count;
    std::string pcap, out;

    po::options_description desc("Allowed options");
    desc.add_options()
    ("help,h", "show this help and exit")
    ("output,o", po::value(&out)->value_name("OFILE"),
    "output file, if not specificied stdout is used")
    ("chain", po::value(&state_count)->value_name("STATES"), "constructs a PA from "
    "chained frequency automaton (FFA)")
    ("pcap", po::value(&pcap)->value_name("PCAP")->required(), "pcap file")
    ("count,c", po::value(&count)->value_name("COUNT")->default_value(10000),
    "number of samples (packets) used for constructing FFA")
    ("position,p", po::value<unsigned long>()->value_name("POS"), "ignoring first POS packets")
    ("max,m", po::value<unsigned long>()->value_name("MAX"), "maximal prefix length of sample")
    ("threshold,t", po::value(&threshold)->value_name("THRESH")->default_value(1),
    "threshold parameter for merging algorithm")
    ("alpha,a", po::value(&alpha)->value_name("ALPHA")->default_value(0.05),
    "alpha parameter for merging algorithm");

    po::variables_map varmap;
    try {
        po::store(po::parse_command_line(argc, argv, desc), varmap);
        if (argc == 1 || varmap.count("help")) {
            std::cout << help_str;
            std::cout << desc << std::endl;
            return 0;
        }
        po::notify(varmap);
        conflict_options(varmap, "chain", "alpha");
        conflict_options(varmap, "chain", "max");
        conflict_options(varmap, "chain", "threshold");
    }
    catch (std::exception &e) {
        std::cerr << "Error: " << e.what() << std::endl;
        return 1;
    }

    unsigned long max = varmap.count("max") ? varmap["max"].as<unsigned long>() : 0;
    double mp = 0.02;

    // instantiating a FFA, 255 is maximal value of a symbol, template parameter is symbol type
    FDFA ffa{255};
    PcapReader pcap_reader{pcap};

    // read some packets but do not process them
    if (varmap.count("position")) {
        pcap_reader.omit_packets(varmap["position"].as<unsigned long>());
    }

    if (varmap.count("chain")) {
        make_chain(pcap_reader, ffa, count, state_count);
        mp = 0;
    }
    else {
        make_prefix_tree(pcap_reader, ffa, count, max);
        // function object constructor
        Alergia alg{alpha, threshold};
        // getting a learned FFA
        alg(&ffa);
    }

    // transforming to PDFA
    auto pa = ffa.get_pdfa(mp);
    int ret = 0;
    if (varmap.count("output")) {
        std::ofstream ofile{out};
        if (ofile.is_open()) {
            pa->print(ofile);
            ofile.close();
        }
        else {
            std::cerr << "Error: cannot open " << out << std::endl;
            ret = 2;
        }
    }
    else {
        pa->print();
    }

    delete pa;

    return ret;
}
