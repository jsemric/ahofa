/// @author Jakub Semric
///
/// DSAI algorithm template class.
/// @file dsai.h
///
/// Unless otherwise stated, all code is licensed under a
/// GNU General Public Licence v2.0

#ifndef __dsai_h
#define __dsai_h

#include <cmath>
#include <set>
#include <vector>
#include <algorithm>
#include <cassert>
#include "merging_algorithm.h"

/// Merging algorithm DSAI template class which implements abstract class MergingAlgorithm.
/// Instances of this class are treated as function objects. DSAI implements compatibility test
/// based on finding distinguishing strings. NOT SUITABLE for learning the traffic.
///
class DSAI: public MergingAlgorithm
{
private:
    /// string accepted from specific blue state
    std::vector<std::pair<std::vector<uint8_t>, long double>> qb_strings;

protected:
    virtual bool compatibility_test(unsigned long s1, unsigned long s2) const override;
    virtual void merge(unsigned long qr, unsigned long qb) override;
    virtual bool find_blue(unsigned long &qb) override;
    virtual bool get_strings(unsigned long qb, std::vector<uint8_t> &prefix, long double prob);

public:
    DSAI(long double alpha = 0.05, unsigned treshold = 1);
    virtual ~DSAI();
};

#endif
