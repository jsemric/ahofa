/// @author Jakub Semric
///
/// Merging algorithm abstract class.
/// @file merging_algorithm.h
///
/// Unless otherwise stated, all code is licensed under a
/// GNU General Public Licence v2.0

#ifndef __merging_algorithm_h
#define __merging_algorithm_h

#include <map>
#include <set>
#include <unordered_set>
#include <vector>
#include <algorithm>
#include <cassert>
#include <exception>
#include <ctime>
#include "aux.h"
#include "ffa.h"


class MergingAlgorithmException : public std::exception
{
private:
    std::string error_message;

public:
    MergingAlgorithmException(std::string str="invalid usage of merging algorithm") :
        error_message{str} {};

    virtual const char* what() const throw()
    {
        return error_message.c_str();
    }
};

/// Abstract class making interface for derived merging algorithms and implementing some basic steps
/// as state merging and folding. Any instance of derived classes are treated as function objects.
///
/// @tparam uint8_t type for alphabet symbol.
class MergingAlgorithm
{
protected:
    /// Alpha parameter which gives a rate of merging. Supposed to be between 0 and 0.5
    long double alpha;
    /// the minimal value of frequency for state to be tested with compatibility test.
    unsigned treshold;
    /// Frequency automaton.
    FDFA *data;
    /// set of red states
    std::unordered_set<unsigned long> red;
    /// set of blue states
    std::set<unsigned long> blue;
    /// blue states predecessors
    std::map<unsigned long, std::pair<unsigned long, uint8_t>> blue_predecessors;

protected:
    virtual void stochastic_merge(unsigned long s1, unsigned long s2, unsigned long qf,
                                  uint8_t a);
    virtual void stochastic_fold(unsigned long s1, unsigned long s2);
    virtual void stochastic_fold_no_red(unsigned long s1, unsigned long s2);
    virtual bool compatibility_test(unsigned long s1, unsigned long s2) const = 0;
    virtual void merge(unsigned long qr, unsigned long qb) = 0;
    virtual bool find_blue(unsigned long &qb);

    bool is_red(unsigned long s) const { return red.find(s) != red.end(); };
    bool is_blue(unsigned long s) const { return blue.find(s) != blue.end(); };
    void blue_add(unsigned long s, unsigned long qf, uint8_t a);
    void blue_remove(unsigned long s);

public:
    MergingAlgorithm(long double alpha = 0.05, unsigned treshold = 1);
    virtual ~MergingAlgorithm();

    virtual void operator() (FDFA *ffa);
    virtual void set_alpha(long double a);
    virtual void set_treshold(unsigned t) noexcept;
};

#endif
