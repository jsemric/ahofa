#include <vector>
#include <tuple>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>
#include <cassert>

#include "pdfa.h"
#include "aux.h"

PDFA::PDFA(unsigned alphabet_max) : FiniteAutomaton{alphabet_max}, shift(ceil(log2(alphabet_max+1)))
{

}

PDFA::~PDFA()
{

}

void PDFA::print_transitions(std::ostream &out) const
{
    for (unsigned long i = 0; i <= this->state_max; i++) {
        size_t seg = i << shift;

        for (unsigned j = 0; j <= this->alphabet_max; j++) {
            ProbPath<long unsigned, long double> tmp = transitions[seg + j];
            char buf[128] = "";
            snprintf(buf, 127, "%lu %lu %u %.17Lg\n", i, tmp.state, j, tmp.prob);
            out << buf;
        }
   }
}

void PDFA::print_final_probabilities(std::ostream &out) const
{
    for (unsigned long i = 0; i <= this->state_max; i++) {
        char buf[128] = "";
        snprintf(buf, 127, "%lu %.17Lg\n", i, this->final_probabilities[i]);
        out << buf;
    }
}

void PDFA::print(std::ostream &out) const
{
    print_transitions(out);
    print_final_probabilities(out);
}

/// Builds a PDFA from given vectors.
///
/// @param trans Vector representing transitions.
/// @param fprobs Vector representing final probabilities of the states.
void PDFA::build(
    std::vector<std::map<uint8_t,ProbPath<unsigned long, long double>>> &&trans,
    std::vector<long double> &&fprobs)
{
    this->state_max = trans.size() - 1;

    std::vector<ProbPath<unsigned long, long double>> new_trans(trans.size()*(1 << shift));

    for (size_t i = 0; i < trans.size(); i++) {
        size_t seg = i << shift;
        assert(i == 0 || seg > i);

        for (auto &j : trans[i]) {
            new_trans[seg + j.first] = j.second;
        }
    }

    transitions = std::move(new_trans);
    this->final_probabilities = std::move(fprobs);
}

/// Validates if vectors defining PDFA are properly defined.
///
/// @throw Throws FiniteAutomatonException if given vectors do not specify valid PDFA.
/// @param trans Vector representing transitions.
/// @param fprobs Vector representing final probabilities of the states.
void PDFA::check(
    std::vector<std::map<uint8_t,ProbPath<unsigned long, long double>>> &trans,
    std::vector<long double> &fprobs) const
{
    assert(fprobs.size() == trans.size());

    std::vector<long double> probs(trans.size());
    for (size_t i = 0; i < fprobs.size(); i++) {
        for (auto &j : trans[i]) {
            if (j.second.prob < 0 || j.second.prob > 1) {
                throw FiniteAutomatonException{
                    "invalid probability: " + std::to_string(j.second.prob)};
            }
            probs[i] += j.second.prob;
        }

        if (fprobs[i] < 0 || fprobs[i] > 1) {
            throw FiniteAutomatonException{"invalid probability: " + std::to_string(fprobs[i])};
        }
        probs[i] += fprobs[i];
    }

    // check if well defined
    for (size_t i = 0; i <  probs.size(); i++) {
        if (probs[i] > 1.000001 || probs[i] < 0.999999 ) {
            throw FiniteAutomatonException{
                "invalid sum of probabilities of state " + std::to_string(i) + " sum: "
                + std::to_string(probs[i])};
        }
    }
}

/// Normalizes the probabilities of transitions.
void PDFA::normalize()
{
    for (size_t i = 0; i <= this->state_max; i++) {
        long double sum = 0;
        size_t seg = i << shift;
        for (size_t j = 0; j <= this->alphabet_max; j++) {
            sum += transitions[seg + j].prob;
        }
        sum += this->final_probabilities[i];
        if (i > 1.00000001 || i < 0.99999999) {
            for (size_t j = 0; j <= this->alphabet_max; j++) {
                transitions[seg + j].prob /= sum;
            }
            this->final_probabilities[i] /= sum;
        }
    }
}

/// Reads from file and builds a PDFA.
///
/// @throw Throws FiniteAutomatonException if PDFA is improperly specified.
/// @param input Input file stream.
void PDFA::read(std::ifstream &input)
{
    std::string buf;
    std::set<unsigned long> state_set;
    std::map<unsigned long, unsigned long> state_map;
    std::vector<Triple<unsigned long, uint8_t, ProbPath<unsigned long, long double>>> lines;

    // reading transitions
    while (std::getline(input, buf)) {
        std::istringstream iss(buf);
        unsigned s1, s2, a;
        long double p;
        if (!(iss >> s1 >> s2 >> a >> std::setprecision(20) >> p)) {
            break;
        }

        if (a > this->alphabet_max) {
            throw FiniteAutomatonException{"invalid alphabet symbol"};
        }

        lines.push_back(Triple<unsigned long, uint8_t, ProbPath<unsigned long, long double>>{
            s1,static_cast<uint8_t>(a),{s2,p}});
        state_set.insert(s1);
        state_set.insert(s2);
    }

    // mapping states
    unsigned long state_count = 0;
    for (auto i : state_set) {
        state_map[i] = state_count++;
    }

    // setting transitions
    std::vector<std::map<uint8_t,ProbPath<unsigned long, long double>>> trans(state_count);
    for (auto i : lines) {
        i.third.state = state_map[i.third.state];
        trans[state_map[i.first]][i.second] = i.third;
    }

    this->initial_state = 0;

    std::vector<long double> fprobs(state_count);
    // reading final probabilities
    do {
        std::istringstream iss(buf);
        unsigned s;
        long double p;
        if (!(iss >> s >> p)) {
            throw FiniteAutomatonException{"invalid syntax"};
        }
        fprobs[state_map.at(s)] = p;
    } while (std::getline(input, buf));

    check(trans, fprobs);
    build(std::move(trans), std::move(fprobs));
}
