/// @author Jakub Semric
///
/// Template abstract class for automata and exception class.
/// @file finite_automata.h
///
/// Unless otherwise stated, all code is licensed under a
/// GNU General Public Licence v2.0

#ifndef __finite_automata_h
#define __finite_automata_h

#include <exception>
#include <iostream>
#include <vector>

class FiniteAutomatonException : public std::exception
{
private:
    std::string errorMessage;

public:
    FiniteAutomatonException(std::string str="invalid construction of automata") :
        errorMessage{str} {};

    virtual const char* what() const throw()
    {
        return errorMessage.c_str();
    }
};

/// Abstract template class which creates unified interface for all derived classes.
/// Type variable @b SymbolType allows different numeric representation of alphabet symbols.
/// States respectively symbols are numbered from zero to @b state_max respectively @b alphabet_max.
///
/// @tparam SymbolType type of the alphabet symbol
class FiniteAutomaton
{
protected:
    /// Maximum value of alphabet symbol.
    unsigned alphabet_max;
    /// Numeric value of initial state. Zero preferred.
    unsigned long initial_state;
    /// Highest numeric value of state.
    unsigned long state_max;

public:
    FiniteAutomaton() = default;
    FiniteAutomaton(unsigned alphabet_max);
    virtual ~FiniteAutomaton() = 0;

    unsigned get_alphabet_size() const noexcept { return alphabet_max + 1;};
    unsigned long get_state_size() const noexcept { return state_max + 1;};
    unsigned long get_initial_state() const noexcept { return initial_state;};
    virtual void print(std::ostream &out = std::cout) const  = 0;
};

#endif
