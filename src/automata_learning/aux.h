/// @author Jakub Semric
///
/// Auxiliary data structures and functions.
/// @file aux.h
///
/// Unless otherwise stated, all code is licensed under a
/// GNU General Public Licence v2.0

#ifndef __aux_h
#define __aux_h

#include <iostream>
#include <ctype.h>

template <typename T1, typename T2, typename T3>
struct Triple
{
    T1 first;
    T2 second;
    T3 third;

    Triple() = default;
    Triple(T1 t1, T2 t2, T3 t3) : first{t1}, second{t2}, third{t3} {};
};

template <typename T1, typename T2>
struct ProbPath
{
    T1 state;
    T2 prob;

    ProbPath() = default;
    ProbPath(T1 t1, T2 t2) : state{t1}, prob{t2} {};
};

template <typename T1, typename T2>
struct FreqPath
{
    T1 state;
    T2 freq;

    FreqPath() = default;
    FreqPath(T1 t1, T2 t2) : state{t1}, freq{t2} {};
};

class Measurement
{
private:
    clock_t beg;
public:
    double t;
    Measurement() : beg{0}, t{0} {};
    ~Measurement() = default;
    void start() { beg = clock();};
    void stop() { t += static_cast<double>(clock() - beg) / CLOCKS_PER_SEC;};
    void print() { std::cerr << t << "\n"; };
};

int hex_to_int(const std::string &str);
std::string int_to_hex(const unsigned num);

#endif
