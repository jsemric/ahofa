#include <map>
#include <set>
#include <unordered_set>
#include <vector>
#include <algorithm>
#include <cassert>
#include <exception>
#include <ctime>

#include "merging_algorithm.h"
#include "aux.h"
#include "ffa.h"

MergingAlgorithm::MergingAlgorithm(long double alpha, unsigned treshold) :
    alpha{alpha}, treshold{treshold}, data{nullptr}
{
    if (alpha > 0.5 || alpha < 0) {
        throw MergingAlgorithmException{"invalid parameter alpha"};
    }
}

MergingAlgorithm::~MergingAlgorithm()
{

}

void MergingAlgorithm::set_alpha(long double a)
{
    if (a > 0.5 || a < 0) {
        throw MergingAlgorithmException{"invalid parameter alpha"};
    }

    alpha = a;
}

void MergingAlgorithm::set_treshold(unsigned t) noexcept
{
    treshold = t;
}

/// Redirects a transition @b qf -> @b s2 to @b qf -> @b s1 and folds states @b s1 and @b s2.
///
/// @param s1 new state after redirect
/// @param s2 old state before redirect
/// @param qf state which transition will be redirected
/// @param a symbol which maps qf -> s2
void MergingAlgorithm::stochastic_merge(unsigned long s1, unsigned long s2,
                                                    unsigned long qf, uint8_t a)
{
    assert(s1 != s2);
    assert(data->state_max >= s1);
    assert(data->state_max >= s2);
    assert(data->state_max >= qf);
    assert(data->transitions[qf][a].state = s2);

    // redirect a transition
    data->transitions[qf][a].state = s1;
    // fold states
    stochastic_fold(s1, s2);
}

/// Recursively folds two states. State @b s1 gets transition from @b s2. After moving all
/// transitions of @b s2 are removed.
///
/// @param s1 first state to be folded, supposed to be red one
/// @param s2 second state to be folded
void MergingAlgorithm::stochastic_fold_no_red(unsigned long s1, unsigned long s2)
{
    assert(s1 != s2);
    assert(data->state_max >= s1);
    assert(data->state_max >= s2);
    assert(!is_red(s1));
    assert(!is_blue(s2));

    data->final_frequencies[s1] += data->get_final_frequency(s2);
    for (auto & s2trans : data->transitions[s2]) {
        if (data->transitions[s1].find(s2trans.first) == data->transitions[s1].end()) {
            data->transitions[s1][s2trans.first] = s2trans.second;
        }
        else {
            data->transitions[s1][s2trans.first].freq += s2trans.second.freq;
            stochastic_fold_no_red(data->transitions[s1][s2trans.first].state, s2trans.second.state);
        }
    }

    data->transitions[s2].clear();
}

/// Recursively folds two states. State @b s1 gets transition from @b s2. Because @b s1 is @b red
/// state, all its states from newly added transitions becomes @b blue ones. After moving all
/// transitions of @b s2 are removed.
///
/// @param s1 first state to be folded
/// @param s2 second state to be folded supposed to be neither blue nor red
void MergingAlgorithm::stochastic_fold(unsigned long s1, unsigned long s2)
{
    assert(s1 != s2);
    assert(data->state_max >= s1);
    assert(data->state_max >= s2);
    assert(is_red(s1));

    data->final_frequencies[s1] += data->get_final_frequency(s2);
    for (auto & s2trans : data->transitions[s2]) {
        if (data->transitions[s1].find(s2trans.first) == data->transitions[s1].end()) {
            assert(!is_red(s2trans.second.state));
            blue_add(s2trans.second.state, s1, s2trans.first);
            data->transitions[s1][s2trans.first] = s2trans.second;
        }
        else {
            data->transitions[s1][s2trans.first].freq += s2trans.second.freq;
            if (is_red(data->transitions[s1][s2trans.first].state)) {
                stochastic_fold(data->transitions[s1][s2trans.first].state, s2trans.second.state);
            }
            else {
                stochastic_fold_no_red(data->transitions[s1][s2trans.first].state, s2trans.second.state);
            }
        }
    }

    data->transitions[s2].clear();
    blue_remove(s2);
}

/// Searches for blue state with frequency not lesser than threshold.
///
/// @param qb Location where the value of blue state will be written.
/// @return Returns true if any of the states has greater or equal total frequency than threshold.
bool MergingAlgorithm::find_blue(unsigned long &qb)
{
    for (auto i : blue) {
        assert(data->get_total_frequency(i) > 0);
        if (data->get_total_frequency(i) >= this->treshold) {
            qb = i;
            return true;
        }
    }

    return false;
}

void MergingAlgorithm::blue_add(unsigned long s, unsigned long qf, uint8_t a)
{
    blue.insert(s);
    blue_predecessors[s] = std::pair<unsigned long, uint8_t>{qf,a};
}

/// Removes state @b s from blue states. Predecessors are also removed from blue_predecessors map.
///
/// @param s State to be removed.
void MergingAlgorithm::blue_remove(unsigned long s)
{
    blue.erase(s);
    blue_predecessors.erase(s);
}

/// The core of the algorithm which transforms prefix tree acceptor @b ffa to some general FFA.
/// Consists of three parts. The first part is an initialization. The second one is picking the blue
/// states and testing them. After successful test states are merged (red one with blue one). After
/// no merge blue state is marked as red one. In the last part FFA is normalized by pruning
/// inaccessible states.
///
/// @param ffa Prefix tree acceptor.
void MergingAlgorithm::operator() (FDFA *ffa)
{
    // assignment to member variable
    data = ffa;
    assert (data != nullptr);

    // initializing red states
    red.clear();
    red.insert(data->initial_state);

    // initializing blue states
    blue.clear();
    for (auto & i : data->transitions[data->initial_state]) {
        blue.insert(i.second.state);
        blue_predecessors[i.second.state] = std::pair<unsigned long, uint8_t>(
            data->initial_state, i.first);
    }

    // blue state
    unsigned long qb;

    // main loop
    while (find_blue(qb)) {

        bool merged = false;
        for (auto qr : red) {
            // merging after successful compatibility test of red and blue state
            if (compatibility_test(qr, qb)) {
                merge(qr, qb);
                merged = true;
                break;
            }
        }

        if (!merged) {
            // update red
            red.insert(qb);
            blue_remove(qb);
            for (auto q : data->transitions[qb]) {
                // cannot be in red because blue states are part of prefix tree, thus they can
                // reach red ones
                blue_add(q.second.state, qb, q.first);
            }
        }
    }

    data->remove_uncreachable_states();
    data = nullptr;
}
