/// @author Jakub Semric
///
/// Alergia algorithm template class.
/// @file alergia.h
///
/// Unless otherwise stated, all code is licensed under a
/// GNU General Public Licence v2.0

#ifndef __alergia_h
#define __alergia_h

#include <cmath>
#include <set>
#include <vector>
#include <algorithm>
#include <cassert>
#include "merging_algorithm.h"

/// Merging algorithm Alergia template class which implements abstract class MergingAlgorithm.
/// Instances of this class are treated as function objects. Alergia implements compatibility test
/// by comparing frequencies of two states using Hoeffding bounds.
class Alergia: public MergingAlgorithm
{
private:
    /// Precomputed value for faster evaluation.
    long double alpha_multiple;

protected:
    virtual bool compatibility_test(unsigned long s1, unsigned long s2) const override;
    virtual void merge(unsigned long qr, unsigned long qb) override;
    bool alergia_test(unsigned long f1, unsigned long n1, unsigned long f2, unsigned long n2) const;

public:
    Alergia(long double alpha = 0.05, unsigned treshold = 1);
    virtual ~Alergia();

    virtual void set_alpha(long double a) override;
};

#endif
