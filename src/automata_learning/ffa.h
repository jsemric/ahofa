/// @author Jakub Semric
///
/// Frequency automaton class definition and methods.
/// @file ffa.h
///
/// Unless otherwise stated, all code is licensed under a
/// GNU General Public Licence v2.0

#ifndef __ffa_h
#define __ffa_h

#include <vector>
#include <map>
#include <set>
#include <fstream>
#include <iostream>
#include <cassert>
#include <ctime>
#include <cmath>
#include <cstdlib>

#include "aux.h"
#include "finite_automata.h"
#include "pdfa.h"


class MergingAlgorithm;
class Alergia;
class DSAI;

class FDFA : public FiniteAutomaton
{
private:
    /// Index is a state ID.
    /// Map key is a symbol.
    /// First item of a pair is a destination state
    /// Last one is a frequency.
    std::vector<std::map<uint8_t,FreqPath<unsigned long, unsigned>>> transitions;
    /// Final frequencies.
    std::map<unsigned long, unsigned> final_frequencies;
    /// Number of samples run through automaton.
    unsigned long long samples;
    friend MergingAlgorithm;
    friend Alergia;
    friend DSAI;

private:
    unsigned long long sum_frequencies() const noexcept;
    void check_frequencies() const;
    void compare_total_frequency(unsigned long long old_sum) const;
    void normalize_probs() const;

public:
    FDFA() = default;
    FDFA(unsigned alph_size = 255);
    FDFA(FDFA && ffa);
    ~FDFA();

    inline unsigned get_total_frequency(unsigned long state) const;
    inline unsigned get_final_frequency(unsigned long state) const;
    inline unsigned long get_state(unsigned long state, uint8_t symbol) const;
    inline bool exists(unsigned long state, uint8_t symbol) const;
    virtual void print(std::ostream &out = std::cout) const noexcept override;
    std::string visualize() const noexcept;
    unsigned long add_state();
    void set_final_frequency(unsigned long state, unsigned freq);
    void add_transition(unsigned long pstate, uint8_t symbol, unsigned long qstate,
                        unsigned freq);

    template<typename U>
    inline long double accept(const U word, unsigned length) const;

    template<typename U>
    inline long double accept(const U word, unsigned length, unsigned long actual) const;

    template<typename U>
    inline void augment(const U word, unsigned length);

    PDFA *get_pdfa(long double merging_prior = 0.02) const;
    void remove_uncreachable_states();
};


/// Search for the state in @b final_frequencies vector and returns its final frequency.
///
/// @param state Number of state.
/// @return final frequency of the state
inline unsigned FDFA::get_final_frequency(unsigned long state) const
{
    assert(state <= this->state_max);
    return final_frequencies.find(state) == final_frequencies.end() ?
           0 : final_frequencies.at(state);
}

/// Computes a total frequency of state including final frequency.
///
/// @param state Number of state.
/// @return Total outgoing frequency of state @b state.
inline unsigned FDFA::get_total_frequency(unsigned long state) const
{
    assert(state <= this->state_max);

    unsigned res = 0;
    for (auto &i : transitions[state]) {
        res += i.second.freq;
    }

    return res + get_final_frequency(state);
}

/// Returns a state reachable from @b state by @b symbol.
///
/// @throw If transitions does not exists exception std::out_of_range is raised.
/// @param state Number of state.
/// @param symbol Number of symbol.
/// @return A value of state reachable from @b state by @b symbol.
inline unsigned long FDFA::get_state(unsigned long state, uint8_t symbol)
const
{
    assert(state <= this->state_max);
    return transitions[state].at(symbol).state;
}


/// Augments the ffa by compiling a word in the same way as prefix tree acceptor. If a suffix of
/// word is not accepted than states and transitions are created to achieve accepting of such word.
///
/// @tparam U type of container or array which represents the word. Type must support indexing.
/// @param word string to be compiled.
/// @param length of the string.
template<typename U>
inline void FDFA::augment(const U word, unsigned length)
{
    this->samples++;
    unsigned long actual = this->initial_state;
    unsigned idx = 0;
    while (idx < length) {
        if (transitions[actual].find(word[idx]) != transitions[actual].end()) {
            transitions[actual][word[idx]].freq++;
            actual = transitions[actual][word[idx]].state;
            idx++;
        }
        else {
            // add new branch
            transitions[actual][word[idx++]] = FreqPath<unsigned long, unsigned>
                                               {++this->state_max,1};

            transitions.push_back(std::map<uint8_t,FreqPath<unsigned long, unsigned>>{});

            while (idx < length) {

                actual = this->state_max;
                transitions[actual][word[idx++]] = FreqPath<unsigned long,unsigned>
                                                   {++this->state_max,1};

                transitions.push_back(std::map<uint8_t,FreqPath<unsigned long,unsigned>>{});

            }

            actual = this->state_max;
        }
    }

    final_frequencies[actual]++;
}

/// Returns a probability of a given word. If the word is not accepted by FFA then zero is returned.
///
/// @tparam U type of container or array which represents the word. Type must support indexing.
/// @param word string to be compiled.
/// @param length of the string.
/// @return Return a frequency if word is accepted or zero if not.
template<typename U>
inline long double FDFA::accept(const U word, unsigned length) const
{
    return accept(word, length, this->initial_state);
}

/// Returns a probability of a given word. If the word is not accepted by FFA then zero is returned.
///
/// @tparam U type of container or array which represents the word. Type must support indexing.
/// @param word string to be compiled.
/// @param length of the string.
/// @return Return a frequency if word is accepted or zero if not.
template<typename U>
inline long double FDFA::accept(const U word, unsigned length,
                                                    unsigned long actual) const
{
    long double res = 1;
    for (unsigned i = 0; i < length; i++) {
        if (transitions[actual].find(word[i]) != transitions[actual].end()) {
            res *= 1.0*transitions[actual].at(word[i]).freq / get_total_frequency(actual);
            actual = transitions[actual].at(word[i]).state;
        }
        else {
            return 0;
        }
    }

    return res * get_final_frequency(actual)/get_total_frequency(actual);
}

/// Check whether exists a path from @b state by @b symbol.
///
/// @param state Number of state.
/// @param state Number of symbol.
/// @return Boolean value indicating of existence transition from @b state by @b symbol.
inline bool FDFA::exists(unsigned long state, uint8_t symbol) const
{
    return transitions[state].find(symbol) != transitions[state].end();
}

#endif
