/// @author Jakub Semric
///
/// Alergia algorithm template class.
/// @file alergia.h
///
/// Unless otherwise stated, all code is licensed under a
/// GNU General Public Licence v2.0

#include <cmath>
#include <set>
#include <vector>
#include <algorithm>
#include <cassert>

#include "ffa.h"
#include "alergia.h"

Alergia::~Alergia()
{

}


Alergia::Alergia(long double alpha, unsigned treshold) :
    MergingAlgorithm(alpha, treshold)
{
    alpha_multiple = sqrt(log(2.0/alpha))/1.41421356237309504880;
}


void Alergia::set_alpha(long double a)
{
    MergingAlgorithm::set_alpha(a);
    alpha_multiple = sqrt(log(2.0/alpha))/1.41421356237309504880;
}

/// Action which takes place after successful compatibility test. In this case it is just simple
/// merging of two states.
///
/// @param qr A state supposed to be @b red one.
/// @param qb A state supposed to be @b blue one.
void Alergia::merge(unsigned long qr, unsigned long qb)
{
    stochastic_merge(qr, qb, blue_predecessors[qb].first,
                           blue_predecessors[qb].second);
}

/// Compares final and transition frequencies of given states using Hoeffding bounds in
/// Alergia test.
///
/// @return Returns true if two states are compatible, hence suitable for merge.
bool Alergia::compatibility_test(unsigned long s1, unsigned long s2) const
{
    assert (s1 != s2);

    if (!alergia_test(data->get_final_frequency(s1), data->get_total_frequency(s1),
                      data->get_final_frequency(s2), data->get_total_frequency(s2)))
    {
        return false;
    }

    for (uint8_t i = 0; i < data->alphabet_max; i++) {
        if (data->exists(s1, i) && data->exists(s2, i)) {
            if (!alergia_test(data->transitions[s1][i].freq,
                              data->get_total_frequency(s1),
                              data->transitions[s2][i].freq,
                              data->get_total_frequency(s2)))
            {
                return false;
            }
        }
    }

    return true;
}

/// Testing the frequencies using Hoeffding bounds.
///
/// @return Returns true if input frequencies are sufficiently close.
bool Alergia::alergia_test(unsigned long f1,unsigned long n1, unsigned long f2,
        unsigned long n2) const
{
    assert(n1 > 0 && n2 > 0);
    double gamma, bound;
    gamma = std::fabs(1.0*f1/n1 - 1.0*f2/n2);
    bound = (sqrt(1.0/n1) + sqrt(1.0/n2)) * alpha_multiple;

    return bound > gamma;
}
