/// @author Jakub Semric
///
/// Probabilistic automaton template class definition and methods.
/// @file pdfa.h
///
/// Unless otherwise stated, all code is licensed under a
/// GNU General Public Licence v2.0

#ifndef __nfa_h
#define __nfa_h

#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <map>
#include <set>
#include "finite_automata.h"
#include "aux.h"

class NFA : public FiniteAutomaton
{
private:
    /// state + symbol = set of states
    std::vector<std::vector<unsigned long>> transitions;
    std::set<unsigned long> final_states;
    std::vector<unsigned long> state_freq;
    std::vector<unsigned> state_depth;
    std::vector<bool> visited_states;

    unsigned shift = 8;

public:
    NFA(unsigned alphabet_max = 255);
    ~NFA();

    template<typename U>
    inline bool accept(const U word, unsigned length) const;

    template<typename U>
    inline void compute_frequency(const U word, unsigned length);

    void print(std::ostream &out = std::cout) const;
    void print_freq(std::ostream &out = std::cout) const;
    void read_from_file(std::ifstream &input);
    void compute_depth();

private:
    void update_freq() noexcept;
};

template<typename U>
inline bool NFA::accept(const U word, unsigned length) const
{
    std::set<unsigned long> actual{initial_state};

    for (unsigned i = 0; i < length && !actual.empty(); i++) {
        std::set<unsigned long> tmp;
        for (auto j : actual) {
            assert ((j << shift) + word[j] < transitions.size());
            auto trans = transitions[(j << shift) + word[i]];
            if (!trans.empty()) {
                for (auto k : trans) {
                    if (final_states.find(k) != final_states.end()) {
                        return true;
                    }
                    tmp.insert(k);
                }
            }
        }
        actual = std::move(tmp);
    }

    return false;
}

template<typename U>
inline void NFA::compute_frequency(const U word, unsigned length)
{
    std::set<unsigned long> actual{initial_state};

    for (unsigned i = 0; i < length && !actual.empty(); i++) {
        std::set<unsigned long> tmp;
        for (auto j : actual) {
            assert ((j << shift) + word[j] < transitions.size());
            auto trans = transitions[(j << shift) + word[i]];
            if (!trans.empty()) {
                for (auto k : trans) {
                    visited_states[k] = true;
                    /*  assuming that packet cannot leave a final state
                    if (final_states.find(k) != final_states.end()) {
                        update_freq();
                        return;
                    }
                    */
                    tmp.insert(k);
                }
            }
        }
        actual = std::move(tmp);
    }

    update_freq();
}

inline void NFA::update_freq() noexcept
{
    for (size_t i = 0; i <= state_max; i++) {
        state_freq[i] += visited_states[i];
        visited_states[i] = false;
    }
}

#endif
