/// @author Jakub Semric
///
/// Auxiliary data structures and functions.
/// @file aux.h
///
/// Unless otherwise stated, all code is licensed under a
/// GNU General Public Licence v2.0

#include <iostream>
#include <cassert>
#include <ctype.h>

#include "aux.h"

int hex_to_int(const std::string &str)
{
    int x = 0;
    int m = 1;
    for (int i = str.length()-1; i > 1; i--) {
        int c = tolower(str[i]);
        x += m * (c - (c > '9' ? 'a' - 10 : '0'));
        m *= 16;
    }

    return x;
}

std::string int_to_hex(const unsigned num)
{
    assert (num <= 255);
    char buf[16] = "";
    sprintf(buf, "0x%.2x", num);
    return buf;
}
