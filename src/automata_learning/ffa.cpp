#include <vector>
#include <map>
#include <set>
#include <fstream>
#include <iostream>
#include <cassert>
#include <ctime>
#include <cmath>
#include <cstdlib>

#include "aux.h"
#include "ffa.h"
#include "pdfa.h"


FDFA::FDFA(unsigned alph_size) : FiniteAutomaton{alph_size}, samples{0}
{
    transitions.push_back(std::map<uint8_t,FreqPath<unsigned long, unsigned>>{});
}


/// Move constructor.
FDFA::FDFA(FDFA && ffa)
{
    transitions = std::move(ffa.transitions);
    final_frequencies = std::move(ffa.final_frequencies);
    this->initial_state = ffa.initial_state;
    this->alphabet_max = ffa.alphabet_max;
    this->state_max = ffa.state_max;
    samples = ffa.samples;
}

FDFA::~FDFA()
{

}

unsigned long FDFA::add_state()
{
    transitions.push_back(std::map<uint8_t,FreqPath<unsigned long, unsigned>>{});
    return ++this->state_max;
}

void FDFA::set_final_frequency(unsigned long state, unsigned freq)
{
    if (state > this->state_max)
    {
        throw FiniteAutomatonException{"invalid transition"};
    }

    final_frequencies[state] = freq;
}

void FDFA::add_transition(unsigned long pstate, uint8_t symbol,
                                              unsigned long qstate, unsigned freq)
{
    if (pstate > this->state_max || qstate > this->state_max || symbol > this->alphabet_max
        || !freq)
    {
        throw FiniteAutomatonException{"invalid transition"};
    }

    transitions[pstate][symbol] = FreqPath<unsigned long, unsigned>{qstate, freq};
}

/// Prints a structure of automata in a dot format.
std::string FDFA::visualize() const noexcept
{

    std::string res = "digraph \" Automata \" {\n\trankdir=LR;\n"
                      "\tnode [shape = circle];\n";

    for (unsigned long state = 0; state <= this->state_max; state++) {
        for (auto trans : transitions[state]) {
            res += "\tq" + std::to_string(state) + "_" + std::to_string(get_final_frequency(state));
            res += " -> q" + std::to_string(trans.second.state) + "_"
                   + std::to_string(get_final_frequency(trans.second.state));
            res += " [ label = \"" + std::to_string(trans.first) + "/"
                   + std::to_string(trans.second.freq) + "\" ];\n";
        }
    }

    return res + "}\n";
}

/// Prints a structure of automata in the following syntax. First prints all transitions of
/// format @b source_state @b symbol @b destination_state $b frequency. After all transition printed
/// the final frequencies are displayed.
void FDFA::print(std::ostream &out) const noexcept
{
    for (unsigned long i = 0; i <= this->state_max; i++) {
        for (auto & j : transitions[i]) {
            out << i << " " << (unsigned)j.first << " " << j.second.state << " "
                << j.second.freq << "\n";
        }
    }

    for (auto i : final_frequencies) {
        out << i.first << " " << i.second << "\n";
    }
}

/// Removes states not reachable from initial state.
/// This method is supposed to be called by merging algorithm after merging the states.
void FDFA::remove_uncreachable_states()
{

    std::map<unsigned long, unsigned long> state_map;
    std::set<unsigned long> state_set{this->initial_state};
    std::set<unsigned long> actual{this->initial_state};
    unsigned long new_state_count = 0;

    while (!actual.empty()) {
        std::set<unsigned long> newActual;

        for (auto s : actual) {
            for (auto &i : transitions[s]) {
                if (state_set.find(i.second.state) == state_set.end()) {
                    newActual.insert(i.second.state);
                }
                state_set.insert(i.second.state);
            }
        }

        actual = std::move(newActual);
    }

    for (auto i : state_set) {
        state_map[i] = new_state_count++;
    }

    std::vector<std::map<uint8_t,FreqPath<unsigned long, unsigned>>>new_trans(new_state_count);

    for (auto i : state_set) {
        assert(state_map[i] < new_state_count);
        for (auto & j : transitions[i]) {
            new_trans[state_map[i]][j.first] =
                FreqPath<unsigned long, unsigned>{state_map[j.second.state], j.second.freq};
        }
    }

    transitions = std::move(new_trans);
    this->state_max = new_state_count - 1;
    this->initial_state = state_map[this->initial_state];


    std::map<unsigned long, unsigned> new_final_fr;

    for (auto i : state_set) {
        if (final_frequencies[i] > 0) {
            new_final_fr[state_map[i]] = final_frequencies[i];
        }
    }

    final_frequencies = std::move(new_final_fr);
}

/// Normalizes a frequency automaton to probabilistic automaton.
///
/// @param merging_prior parameter which gives a bias to creating probabilities from frequencies.
/// Default value is 0.02.
/// @return Returns a probabilistic automaton.
PDFA *FDFA::get_pdfa(long double merging_prior) const
{
    PDFA *ret = new PDFA{this->alphabet_max};
    std::vector<std::map<uint8_t,ProbPath<unsigned long, long double>>>
    rules(this->state_max + 1);

    std::vector<long double> final_probs(this->state_max + 1);
    unsigned alph_size = this->alphabet_max;

    for (size_t i = 0; i <= this->state_max; i++) {
        long double totalFreq = get_total_frequency(i);
        for (unsigned a = 0; a <= alph_size; a++) {
            if (exists(i, a)) {
                long double prob = (transitions[i].at(a).freq + merging_prior ) /
                                   (totalFreq + merging_prior * (alph_size + 2));

                rules[i][a] = ProbPath <unsigned long, long double>{
                    transitions[i].at(a).state, prob};
            }
            else {
                long double prob = merging_prior / (totalFreq + merging_prior * (alph_size + 2));

                rules[i][a] = ProbPath <unsigned long, long double>{i, prob};
            }
        }

        final_probs[i] = (get_final_frequency(i) + merging_prior) /
                        (totalFreq + merging_prior * alph_size);
    }

    ret->build(std::move(rules), std::move(final_probs));
    ret->normalize();

    return ret;
}

/// Sum all outgoing frequencies of all states.
///
/// @return total frequency of all states.
unsigned long long FDFA::sum_frequencies() const noexcept
{
    unsigned long long totalFreq = 0;
    for (size_t i = 0; i <= this->state_max; i++) {
        totalFreq += get_total_frequency(i);
    }

    return totalFreq;
}

/// Test if frequency automaton is well defined.
///
/// @throw Raises a FiniteAutomatonException if incoming and outgoing frequencies of any states
/// are not equal
void FDFA::check_frequencies() const
{
    std::vector<unsigned long> incoming(this->state_max + 1);
    std::vector<unsigned long> outcomming(this->state_max + 1);

    for (size_t i = 0; i <= this->state_max; i++) {
        for (auto &j : transitions[i]) {
            outcomming[i] += j.second.freq;
            incoming[j.second.state] += j.second.freq;
        }
        outcomming[i] += get_final_frequency(i);
    }

    incoming[this->initial_state] += samples;

    for (size_t i = 0; i <= this->state_max; i++) {
        if (outcomming[i] != incoming[i]) {
            std::cerr << i << "\n";
            std::cerr << outcomming[i] << " " << incoming[i] << "\n";
            throw FiniteAutomatonException{"invalid frequencies after merging algorithm"};
        }
    }
}

void FDFA::compare_total_frequency(unsigned long long old_sum) const
{
    if (old_sum != sum_frequencies()) {
        throw FiniteAutomatonException{"invalid frequencies after merging algorithm"};
    }
}
