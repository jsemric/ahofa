#include <cmath>
#include <set>
#include <vector>
#include <algorithm>
#include <cassert>

#include "ffa.h"
#include "dsai.h"

DSAI::~DSAI()
{

}


DSAI::DSAI(long double alpha, unsigned treshold) :
    MergingAlgorithm(alpha, treshold)
{

}


bool DSAI::get_strings(unsigned long state, std::vector<uint8_t> &prefix,
                                   long double prob)
{
    long double total = data->get_total_frequency(state);
    for (auto &i : data->transitions[state]) {
        auto new_prefix = prefix;
        new_prefix.push_back(i.first);
        // string with probability higher than 0.5 reached
        if (!get_strings(i.second.state, new_prefix, prob*i.second.freq/total)) {
            break;
        }
    }

    long double fpr = 0;
    if ((fpr = data->get_final_frequency(state))) {
        qb_strings.push_back(std::pair<std::vector<uint8_t>, long double>
                             {prefix, prob*(fpr/total)});
        return prob*(fpr/total) < 0.5;
    }

    return true;
}

/// Searches for blue state with frequency not lesser than threshold.
///
/// @param qb Location where the value of blue state will be written.
/// @return Returns true if any of the states has greater or equal total frequency than threshold.
bool DSAI::find_blue(unsigned long &qb)
{
    if (MergingAlgorithm::find_blue(qb)) {
        qb_strings.clear();
        std::vector<uint8_t> p{};
        get_strings(qb, p, 1.0);

        for (auto &i : qb_strings) {
            assert (data->accept(i.first, i.first.size(), qb) > i.second - 0.001);
            assert (data->accept(i.first, i.first.size(), qb) < i.second + 0.001);
        }

        return true;
    }

    return false;
}

/// Action which takes place after successful compatibility test. In this case it is just simple
/// merging of two states.
///
/// @param qr A state supposed to be @b red one.
/// @param qb A state supposed to be @b blue one.
void DSAI::merge(unsigned long qr, unsigned long qb)
{
    stochastic_merge(qr, qb, blue_predecessors[qb].first,
                           blue_predecessors[qb].second);
}


bool DSAI::compatibility_test(unsigned long s1, unsigned long s2) const
{
    assert (s1 != s2);
    (void) s2;

    for (auto &i : qb_strings) {
        long double s1_prob = data->accept(i.first, i.first.size(), s1);
        if (std::fabs(s1_prob - i.second) > alpha/2) {
            return false;
        }
    }

    return true;
}
