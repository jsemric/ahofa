/// @author Jakub Semric
///
/// Probabilistic automaton template class definition and methods.
/// @file pdfa.h
///
/// Unless otherwise stated, all code is licensed under a
/// GNU General Public Licence v2.0

#ifndef __pdfa_h
#define __pdfa_h

#include <vector>
#include <tuple>
#include <map>
#include <set>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>
#include <iomanip>
#include "aux.h"
#include "finite_automata.h"

class PDFA : public FiniteAutomaton
{
private:
    /// PDFA transition. Just plain vector due to faster manipulation.
    std::vector<ProbPath<unsigned long, long double>> transitions;
    /// Value of shifting the index of state.
    unsigned shift;

protected:
    /// Final probability of each state.
    std::vector<long double> final_probabilities;

protected:
    virtual void check(
        std::vector<std::map<uint8_t,ProbPath<unsigned long, long double>>> &trans,
        std::vector<long double> &fprobs) const;

public:
    PDFA(unsigned alphabet_max = 255);
    ~PDFA();

    virtual void print(std::ostream &out = std::cout) const override;
    virtual void print_transitions(std::ostream &out) const;
    virtual void print_final_probabilities(std::ostream &out) const;

    template<typename U>
    long double accept(const U word, unsigned length) const;

    virtual void read(std::ifstream &input);
    virtual void build(
        std::vector<std::map<uint8_t,ProbPath<unsigned long, long double>>> &&trans,
        std::vector<long double> &&fprobs);
    virtual void normalize();
};

template<typename U>
long double PDFA::accept(const U word, unsigned length) const
{
    long double res = 1;                      // initial probability
    size_t act = this->initial_state;         // actual node
    ProbPath<unsigned long, long double> tmp;

    for (unsigned i = 0; i < length; i++) {
        tmp = transitions[(act << shift) + word[i]];
        act = tmp.state;
        res *= tmp.prob;
    }

    return res*this->final_probabilities[act];
}

#endif
