AHOFA - Approximate Handling Of Finite Automata
===============================================

Learning the traffic using merging algorithm Alergia.

Requirements
============

 - **g++** (at least 4.8.5)
 - **lpcap** - http://www.tcpdump.org/
 - C++ **boost_options** library - http://www.boost.org/

How to run
==========

run `make` for compiling

`traffic_learning` for learning and constructing the automata

`pdfa_test` for testing the probabilistic automata with specified packet capture files

`nfa_error` for testing an error between NFAs or for computing state frequencies

`reduce_nfa.py` for NFA reduction
