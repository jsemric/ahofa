CXX=g++
CXXFLAGS=-std=c++11 -Wall -Wextra -pedantic -O3 -DNDEBUG
LIBS=-lpcap -lboost_program_options

SRC_PATH=src
FA_PATH=$(SRC_PATH)/automata_learning
L7_PATH=$(SRC_PATH)/pcap_reader
MAIN_PATH=$(SRC_PATH)/programs

L7_HDR=$(wildcard $(L7_PATH)/*.h)
L7_SRC=$(wildcard $(L7_PATH)/*.cpp)
L7_OBJ=$(patsubst %.cpp, %.o, $(L7_SRC))
FA_HDR=$(wildcard $(FA_PATH)/*.h)
FA_SRC=$(wildcard $(FA_PATH)/*.cpp)
FA_OBJ=$(patsubst %.cpp, %.o, $(FA_SRC))

PROG=pdfa_test
PROG2=traffic_learning
PROG3=nfa_error
MAIN=$(MAIN_PATH)/$(PROG)
MAIN2=$(MAIN_PATH)/$(PROG2)
MAIN3=$(MAIN_PATH)/$(PROG3)

all: $(PROG3)

$(PROG): $(FA_OBJ) $(L7_OBJ) $(MAIN).o
	$(CXX) $(CXXFLAGS) $^ -o $@ $(LIBS)

$(MAIN).o: $(MAIN).cpp
	$(CXX) $(CXXFLAGS) -c $< -o $@ $(LIBS)

%.o: %.cpp %.h
	$(CXX) $(CXXFLAGS) -c $< -o $@ $(LIBS)

$(PROG2): $(FA_OBJ) $(MAIN2).o $(L7_OBJ)
	$(CXX) $(CXXFLAGS)  $^ -o $@ $(LIBS)

$(MAIN2).o: $(MAIN2).cpp $(FA_OBJ)
	$(CXX) $(CXXFLAGS) -c  $< -o $@ $(LIBS)

$(PROG3): $(FA_OBJ) $(MAIN3).o $(L7_OBJ)
	$(CXX) $(CXXFLAGS)   $^ -o $@ $(LIBS)

$(MAIN3).o: $(MAIN3).cpp $(FA_OBJ)
	$(CXX) $(CXXFLAGS) -c  $< -o $@ $(LIBS)

doxygen:
	rm -rf doc
	doxygen $(SRC_PATH)/doxyConf

.PHONY: clean all

test:
	$(MAKE) -C tests

nodata:
	rm -f obs* tmp* *.fsm *.fa *.pa

clean:
	rm -f $(PROG) $(PROG2) $(PROG3) $(L7_PATH)/*.o $(MAIN3).o $(MAIN2).o $(MAIN).o $(FA_PATH)/*.o
	rm -rf doc
